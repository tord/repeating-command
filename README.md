
# Lazy Linux Commands

Features:
* Each command setup has a name for easy recall
* Commands can have parameter placeholders:
  * Random file
  * *Multiple files*: All files in a dir (with or without subdirs)
* Multiple files:
  * recursive (into sub-dirs)
  * matching a pattern (ex `*.jpg`)
* Text output of commands
* (More in ideas.txt)

Usage examples:
* Mass rename of files matching a specific criteria
* Choosing a dir for converting all files (audio or image files), `convert -resize 50% ${current_file_name} ${new_file_name}`
* Showing a random file (ex image file): `xdg-open ${file_name_random}`
* NOT AVAILABE YET: Running a backup script once per week
* NOT AVAILABE YET: Changing the light at 21:00 to lowest level (for laptop computers)
* NOT AVAILABE YET: Changing the light/volume gradually and reaching a set level at 21:00 (for laptop 
  computers)
* NOT AVAILABE YET: Showing notification in the systray to show a random picture from a specified directory at 18:00

<!--
* Go through a list of files. Files are processed one after the other --- waiting on a command to finish for a file before running it for the next one

-->

## Installation


### Using pip


### Binary file


## Screenshots
