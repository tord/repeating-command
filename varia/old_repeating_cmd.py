#!/usr/bin/env python3
import sys
import logging
import configparser
import os
import os.path
import subprocess
import shlex
import concurrent.futures
import time
import datetime
from string import Template
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui


SETTINGS_FILE_NAME_STR = "../file-explorer/settings.ini"
SETTINGS_SECTION_GENERAL_STR = "general"
SETTINGS_SECTION_COMMANDS_STR = "commands"
HISTORY_FILE_NAME_STR = "../file-explorer/history.txt"


def get_strings_from_file(i_file_path: str) -> list:
    string_list = []
    ret_list = []
    if not os.path.isfile(i_file_path):
        with open(i_file_path, "x") as f:
            pass
    else:
        with open(i_file_path, "r") as f:
            string_list = f.readlines()
    for line_str in string_list:
        ret_list.append(line_str.strip())
    return ret_list


def add_string_to_file(i_file_path: str, i_value: str):
    line_list = get_strings_from_file(i_file_path)
    line_list.append(i_value)
    with open(i_file_path, "w") as f:
        f.writelines([line_str + '\n' for line_str in line_list])
        # -please note that writelines doesn't add newline at the end of the line!


def add_string_to_config(i_section: str, i_key: str, i_value: str):
    config_parser = configparser.ConfigParser(interpolation=None, delimiters=["="])
    # config_file_path_str = get_appl_path(SETTINGS_FILE_NAME_STR)
    config_file_path_str = SETTINGS_FILE_NAME_STR
    config_parser.read(config_file_path_str)
    if not config_parser.has_section(i_section):
        config_parser.add_section(i_section)
    config_parser.set(i_section, i_key, i_value)
    with open(config_file_path_str, "w") as file:
        config_parser.write(file)


class MyThreadPoolExecutor(concurrent.futures.ThreadPoolExecutor):
    """
    https://stackoverflow.com/questions/47289751/using-threadpoolexecutor-without-blocking
    """

    # overridden
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.shutdown(wait=False)
        return False


def get_string_from_config(i_section: str, i_key: str, i_default_value: str) -> str:
    config_parser = configparser.ConfigParser(interpolation=None, delimiters=["="])
    # config_file_path_str = get_appl_path(SETTINGS_FILE_NAME_STR)
    config_file_path_str = SETTINGS_FILE_NAME_STR
    config_parser.read(config_file_path_str)
    if not config_parser.has_section(i_section):
        config_parser.add_section(i_section)
    if not config_parser.has_option(i_section, i_key):
        config_parser.set(i_section, i_key, i_default_value)
        with open(config_file_path_str, "w") as file:
            config_parser.write(file)
    ret_value_str = config_parser[i_section][i_key]
    return ret_value_str


def get_dictionary_from_config(i_section: str) -> dict:
    # FAV_PREF_STR
    # , i_default_value: dict
    config_parser = configparser.ConfigParser(interpolation=None, delimiters=["="])
    # config_file_path_str = get_appl_path(SETTINGS_FILE_NAME_STR)
    config_file_path_str = SETTINGS_FILE_NAME_STR
    config_parser.read(config_file_path_str)
    if not config_parser.has_section(i_section):
        config_parser.add_section(i_section)
        with open(config_file_path_str, "w") as file:
            config_parser.write(file)
    ret_value_str = dict(config_parser.items(i_section))
    return ret_value_str


def remove_any_dsuffix(i_whole_text: str) -> str:
    if "." in i_whole_text:
        (before_str, sep_str, after_str) = i_whole_text.rpartition(".")
        return before_str
    return i_whole_text


def _cmd_operation(i_completed_command: str):
    logging.debug(f"Starting {i_completed_command}")
    completed_command_strlist = shlex.split(i_completed_command)
    subprocess.run(completed_command_strlist)
    """
    output_str = subprocess.run(
        completed_command_strlist,
        capture_output=True
    )
    """
    logging.debug(f"Finished {i_completed_command}")

    return f"New Finished {i_completed_command}"


OLD_STR = "${old}"
NEW_STR = "${new}"

# See files: antiword-recursive, batch_rn, and ffmpeg-recursive


class MyMainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.updating_gui_bool = False
        self.center_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.center_widget)

        vbox_l1 = QtWidgets.QVBoxLayout()
        self.center_widget.setLayout(vbox_l1)

        # path_hbox_l2 = QtWidgets.QHBoxLayout()
        # vbox_l1.addLayout(path_hbox_l2)

        self.dir_path_qgb = QtWidgets.QGroupBox("Directory")
        vbox_l1.addWidget(self.dir_path_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.dir_path_qgb.setLayout(vbox_l3)

        # self.path_title_qll = QtWidgets.QLabel("Dir: ")
        # path_hbox_l4.addWidget(self.path_title_qll)
        self.path_sel_qpb = QtWidgets.QPushButton("Select path")
        vbox_l3.addWidget(self.path_sel_qpb)
        self.path_sel_qpb.clicked.connect(self.on_path_sel_clicked)
        self.path_qll = QtWidgets.QLabel("no path selected")
        vbox_l3.addWidget(self.path_qll)
        self.path_qll.setWordWrap(True)
        self.include_subdirs_qcb = QtWidgets.QCheckBox("Include sub-directories")
        vbox_l3.addWidget(self.include_subdirs_qcb)


        self.command_qgb = QtWidgets.QGroupBox("Command")
        vbox_l1.addWidget(self.command_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.command_qgb.setLayout(vbox_l3)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)

        hbox_l4.addWidget(QtWidgets.QLabel("Append to the filename:"))
        self.ending_qcb = QtWidgets.QComboBox()
        hbox_l4.addWidget(self.ending_qcb)
        self.ending_qcb.addItems(["", "_resized"])
        self.ending_qcb.setEditable(True)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)

        hbox_l4.addWidget(QtWidgets.QLabel("Insert new/old filenames:"))
        self.old_qpb = QtWidgets.QPushButton("Old")
        hbox_l4.addWidget(self.old_qpb)
        # self.old_qpb.setCheckable(True)
        self.old_qpb.clicked.connect(self.on_old_clicked)
        self.new_qpb = QtWidgets.QPushButton("New")
        hbox_l4.addWidget(self.new_qpb)
        # self.new_qpb.setCheckable(True)
        self.new_qpb.clicked.connect(self.on_new_clicked)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        hbox_l4.addWidget(QtWidgets.QLabel("Command:"))
        # </b>
        self.cmd_qcb = QtWidgets.QComboBox()
        hbox_l4.addWidget(self.cmd_qcb)
        cmd_list = get_strings_from_file(HISTORY_FILE_NAME_STR)
        # cmd_dict = get_dictionary_from_config(SETTINGS_SECTION_COMMANDS_STR)
        # cmd_values_list = cmd_dict.values() # [v for k, v in cmd_dict.values()]
        # -tricky naming of values here
        self.cmd_qcb.addItems(cmd_list)
        self.cmd_qcb.setEditable(True)
        # self.cmd_qle.textChanged.connect(self.on_cmd_text_changed)

        self.filters_qgb = QtWidgets.QGroupBox("Filters")
        vbox_l1.addWidget(self.filters_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.filters_qgb.setLayout(vbox_l3)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        hbox_l4.addWidget(QtWidgets.QLabel("Suffix:"))
        self.suffix_qcb = QtWidgets.QComboBox()
        hbox_l4.addWidget(self.suffix_qcb)
        self.suffix_qcb.addItems(["", ".ogg", ".jpg"])
        self.suffix_qcb.setEditable(True)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        hbox_l4.addWidget(QtWidgets.QLabel("Contains:"))
        self.contains_qcb = QtWidgets.QComboBox()
        hbox_l4.addWidget(self.contains_qcb)
        # self.contains_qcb.addItems([""])
        self.contains_qcb.setEditable(True)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        hbox_l4.addWidget(QtWidgets.QLabel("Min size:"))
        self.min_size_qsp = QtWidgets.QSpinBox()
        hbox_l4.addWidget(self.min_size_qsp)
        self.min_size_qsp.setMinimum(0)
        self.min_size_qsp.setValue(0)
        self.min_size_type_qcb = QtWidgets.QComboBox()
        hbox_l4.addWidget(self.min_size_type_qcb)
        self.min_size_type_qcb.addItems(["Byte", "kB", "MB"])
        self.min_size_type_qcb.setCurrentText("MB")

        self.process_qgb = QtWidgets.QGroupBox("Process")
        vbox_l1.addWidget(self.process_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.process_qgb.setLayout(vbox_l3)

        mode_hbox_l2 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(mode_hbox_l2)
        self.mode_qbg = QtWidgets.QButtonGroup()
        self.mode_qbg.setExclusive(True)
        self.mode_test_qpb = QtWidgets.QRadioButton("Test")
        self.mode_qbg.addButton(self.mode_test_qpb)
        mode_hbox_l2.addWidget(self.mode_test_qpb)
        self.mode_live_qpb = QtWidgets.QRadioButton("Live")
        self.mode_qbg.addButton(self.mode_live_qpb)
        mode_hbox_l2.addWidget(self.mode_live_qpb)
        self.process_qpb = QtWidgets.QPushButton("Process")
        vbox_l3.addWidget(self.process_qpb)
        self.process_qpb.clicked.connect(self.on_process_clicked)

        self.output_qgb = QtWidgets.QGroupBox("Output")
        vbox_l1.addWidget(self.output_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.output_qgb.setLayout(vbox_l3)

        self.progress_qpb = QtWidgets.QProgressBar(self)
        vbox_l3.addWidget(self.progress_qpb)
        self._set_progress_none()

        self.output_qpte = QtWidgets.QPlainTextEdit()
        vbox_l3.addWidget(self.output_qpte)
        self.output_qpte.setReadOnly(True)

        self.mode_test_qpb.click()

    def _set_progress_none(self):
        self.progress_qpb.setRange(0,1)
        self.progress_qpb.setValue(0)

    def _set_progress_processing(self):
        self.progress_qpb.setRange(0,0)

    def _set_progress_done(self):
        self.progress_qpb.setRange(0, 1)
        self.progress_qpb.setValue(1)

    def on_old_clicked(self):
        self.cmd_qcb.addItem(OLD_STR)

    def on_new_clicked(self):
        self.cmd_qcb.addItem(NEW_STR)
        """
        if self.updating_gui_bool:
            return
        if i_status:
            self.cmd_qle.insert(NEW_STR)
        """

    def on_cmd_text_changed(self, i_new_text: str):
        self.updating_gui_bool = True
        if OLD_STR in i_new_text:
            self.old_qpb.setChecked(True)
        else:
            self.old_qpb.setChecked(False)
        self.updating_gui_bool = False

    def on_process_clicked(self):
        cmd_str = self.cmd_qcb.currentText()

        pydt = datetime.datetime.now()
        pydt_str = str(pydt)

        add_string_to_file(HISTORY_FILE_NAME_STR, cmd_str)
        # add_string_to_config(SETTINGS_SECTION_COMMANDS_STR, pydt_str, cmd_str)

        # Step 1: Extracting the old and new file names from the command string
        # template, fstrings, .format
        # template: ${name_str}
        cmd_template = Template(cmd_str)

        # Building the list of commands to run

        command_strlist = []
        for root, dirs, files in os.walk(self.path_qll.text()):
            for old_file_name_str in files:
                old_file_path_str = os.path.join(root, old_file_name_str)

                # Checking filters

                min_size_int = self.min_size_qsp.value()
                if self.min_size_type_qcb.currentText() == "kB":
                    min_size_in_bytes_int = 1024 * min_size_int
                elif self.min_size_type_qcb.currentText() == "MB":
                    min_size_in_bytes_int = 1024**2 * min_size_int
                else:
                    min_size_in_bytes_int = min_size_int
                file_size_int = os.path.getsize(old_file_path_str)
                if file_size_int < min_size_in_bytes_int:
                    continue

                suffix_str = self.suffix_qcb.currentText()
                if suffix_str.lower() not in old_file_name_str.lower():
                    continue

                contains_str = self.contains_qcb.currentText()
                if contains_str.lower() not in old_file_name_str.lower():
                    continue

                # Assembling the new file name and running the command

                ending_str = self.ending_qcb.currentText()
                new_file_name_str = remove_any_dsuffix(old_file_name_str) + ending_str + suffix_str
                new_dir_path_str = os.path.join(root, new_file_name_str)

                completed_command_str = cmd_template.substitute(
                    old='"' + old_file_path_str + '"',
                    new='"' + new_dir_path_str + '"'
                )

                print(completed_command_str)
                command_strlist.append(completed_command_str)

        if self.mode_test_qpb.isChecked():
            pass
        elif self.mode_live_qpb.isChecked():
            start_time = time.time()
            future_list = []
            """
            for command_str in command_strlist:
                _cmd_operation(command_str)
            """

            # with concurrent.futures.ThreadPoolExecutor() as executor:
            executor = concurrent.futures.ProcessPoolExecutor()
            for command_str in command_strlist:
                future = executor.submit(_cmd_operation, command_str)
                future.add_done_callback(self.update_gui_from_concurrent)
            # executor.shutdown(wait=False)
            executor.shutdown()

            """
            with concurrent.futures.ThreadPoolExecutor() as executor:
                for command_str in command_strlist:
                    future = executor.submit(_cmd_operation, command_str)
                    future.add_done_callback(self.update_gui_from_concurrent)
            """

            """
                future_list.append(future)
            for completed_future in concurrent.futures.as_completed(future_list):
                output_str = completed_future.result()
                # self.output_qpte.appendPlainText(output_str)
            """

            """
            executor = concurrent.futures.ThreadPoolExecutor()
            for command_str in command_strlist:
                future = executor.submit(_cmd_operation, command_str)
                future.add_done_callback(self.update_gui_from_concurrent)
            executor.shutdown(wait=False)
            """

            """
            Option 1:
            for result_str in executor.map(_cmd_operation, command_strlist):
                self.output_qpte.appendPlainText(result_str)
                # TODO: writing to the output_qpte as we process, rather than at the end, see here for info:
                # https://stackoverflow.com/questions/20838162/how-does-threadpoolexecutor-map-differ-from-threadpoolexecutor-submit
            Option 2:
            result_strlist = executor.map(_cmd_operation, command_strlist)
            """



            end_time = time.time()
            logging.debug("==================== Finished all operations ====================")
            time_elapsed = end_time - start_time
            logging.debug(f"Time elapsed: {time_elapsed}")
            # ProcessPoolExecutor: DEBUG:root:Time elapsed: 26.797377347946167
            # ThreadPoolExecutor: DEBUG:root:Time elapsed: 30.492362022399902
            # DEBUG:root:Time elapsed: 44.47875142097473
        self._set_progress_done()

    def update_gui_from_concurrent(self, i_future: concurrent.futures.Future):
        output_str = i_future.result()
        print(f"{output_str=}")
        self.output_qpte.appendPlainText(output_str)
        ########## QtGui.QGuiApplication.processEvents()
        # concurrent.futures.wait()

    def on_path_sel_clicked(self):
        # (path_str, result_bool) = QtWidgets.QFileDialog.getExistingDirectory(None, "Select Dir", "/home")
        # if result_bool:
        # file_dlg = QtWidgets.QFileDialog()

        path_str = QtWidgets.QFileDialog.getExistingDirectory(self, "Select Dir", "/home")
        if path_str:
            self.path_qll.setText(path_str)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)
    main_window = MyMainWindow()

    main_window.show()
    app.exec_()

"""



if same file suffix: warn and overwrite
then we need to make sure that the suffix is the same case (lower/upper)

### showing output dynamically

https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.Executor.shutdown
search terms:
python threadpoolexecutor background blocking GUI

Search: python update gui from worker thread
https://docs.python.org/3/library/concurrent.futures.html

keep old files,
delete,
move to trash directory


examples:
* ffmpeg -i ${old} -c:a libopus -ar 16k -ac 1 -b:a 16k -vbr on -compression_level 10 ${new}
* convert -resize 50% ${old} ${new}
* convert -resize 50% [old] [new]


# Programming

conditional for running the command, for each file. ex: checking file size (this is the most important)

settings file:
adding saved suffixes
adding saved commands

Adding a qle for the new suffix that we want to use

(Difficult: Progress indicator)

Easier: Showing the output in the window

also easier: showing when the application is processing and when it's done

# Ideas

Showing old(existing) and new file names


"""

