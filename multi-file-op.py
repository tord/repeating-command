#!/usr/bin/env python3
import dataclasses
import sys
import os
import subprocess
import random
import stat
from string import Template
# -documentation: https://docs.python.org/3/library/string.html#template-strings
import logging
import json
import fnmatch
from PyQt5 import QtWidgets
from PyQt5 import QtCore
import typing
from PyQt5 import QtGui


FILE_NR_LIMIT = 1000
B_SIZE_STR = "Byte"
KB_SIZE_STR = "kB"
MB_SIZE_STR = "MB"
GB_SIZE_STR = "GB"
CURRENT_FILE_NAME_STR = "${current_file_name}"
NEW_FILE_NAME_STR = "${new_file_name}"
FILE_NAME_RANDOM_STR = "${file_name_random}"
SAVE_FILE_NAME_STR = "saved-setups.json"


# @dataclasses.dataclass
class FilePathCombo:
    def __init__(self, i_old_path: str, i_new_path: str):
        self.old_path_str = i_old_path
        self.new_path_str = i_new_path

    def get_old_path(self) -> str:
        return self.old_path_str

    def get_new_path(self) -> str:
        return self.new_path_str

    def get_old_name(self) -> str:
        return os.path.basename(self.old_path_str)

    def get_new_name(self) -> str:
        return os.path.basename(self.new_path_str)


def remove_suffix(i_text: str, i_suffix: str) -> str:
    # sourcery skip: lift-return-into-if
    # Python 3.9 has added the str method .removesuffix()
    if i_text.endswith(i_suffix) and len(i_suffix) >= 1:
        ret_string = i_text[:-len(i_suffix)]
    else:
        ret_string = i_text
    return ret_string


@dataclasses.dataclass
class SaveAndLoadInfo:
    get_func: typing.Callable
    set_func: typing.Callable
    value_type: typing.Type
    # self.value = i_value


# self.pwd_qll.objectName(): self.pwd_qll.text,

SETUP_CWD_DIR = "cwd_dir"


class MyMainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setMinimumWidth(500)
        self.main_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.main_widget)
        vbox_l2 = QtWidgets.QVBoxLayout()
        self.main_widget.setLayout(vbox_l2)

        self.tray_icon = QtWidgets.QSystemTrayIcon(self)
        icon_file_name_str="icon.png"
        self.tray_icon.setIcon(QtGui.QIcon(icon_file_name_str))
        self.tray_icon.show()
        self.tray_menu = QtWidgets.QMenu(self)
        # self.tray_menu.aboutToShow.connect(self.on_tray_menu_about_to_show)
        self.action_qaction = QtWidgets.QAction("Action")
        self.tray_menu.addAction(self.action_qaction)
        self.tray_icon.setContextMenu(self.tray_menu)


        self.pwd_qgb = QtWidgets.QGroupBox("Working directory")
        vbox_l2.addWidget(self.pwd_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.pwd_qgb.setLayout(vbox_l3)
        self.pwd_qll = QtWidgets.QLabel("-")
        vbox_l3.addWidget(self.pwd_qll, stretch=1)
        self.pwd_qll.setObjectName("cwd_dir")
        self.pwd_qll.setText("/home/sunyata")  # -TODO: User home dir
        self.pwd_qll.setWordWrap(True)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)

        self.set_pwd_qpb = QtWidgets.QPushButton("Set")
        hbox_l4.addWidget(self.set_pwd_qpb)
        self.set_pwd_qpb.setToolTip("Set working dir")
        self.set_pwd_qpb.clicked.connect(self.on_set_pwd_clicked)
        hbox_l4.addStretch()

        """
        # open in terminal
        self.open_in_terminal_qpb = QtWidgets.QPushButton("Terminal")
        hbox_l4.addWidget(self.open_in_terminal_qpb)

        # open in file manager (option: drop-down with "open in" -> [fm, terminal])
        self.open_in_fm_qpb = QtWidgets.QPushButton("File manager")
        hbox_l4.addWidget(self.open_in_fm_qpb)
        """

        self.open_with_qpb = QtWidgets.QPushButton("Open with ...")
        hbox_l4.addWidget(self.open_with_qpb)
        self.open_with_qmenu = QtWidgets.QMenu()
        self.open_with_qpb.setMenu(self.open_with_qmenu)

        self.terminal_qaction = QtWidgets.QAction("Terminal")
        self.open_with_qmenu.addAction(self.terminal_qaction)
        self.terminal_qaction.triggered.connect(self.on_terminal_action_triggered)

        self.file_manager_qaction = QtWidgets.QAction("File manager")
        self.open_with_qmenu.addAction(self.file_manager_qaction)
        self.file_manager_qaction.triggered.connect(self.on_file_manager_action_triggered)

        self.command_qgb = QtWidgets.QGroupBox("Command")
        vbox_l2.addWidget(self.command_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.command_qgb.setLayout(vbox_l3)

        self.command_line_qle = QtWidgets.QLineEdit()
        vbox_l3.addWidget(self.command_line_qle)
        self.command_line_qle.setObjectName("command")
        self.command_line_qle.setPlaceholderText("please enter your command here")
        new_font = self.command_line_qle.font()
        new_font.setPointSize(12)
        self.command_line_qle.setFont(new_font)
        self.command_line_qle.textChanged.connect(self.on_command_line_text_changed)

        vbox_l3.addWidget(QtWidgets.QLabel("Current Filename"))
        self.insert_filename_multi_all_qpb = QtWidgets.QPushButton("Insert filename placeholder, multiple files")
        vbox_l3.addWidget(self.insert_filename_multi_all_qpb)
        self.insert_filename_multi_all_qpb.setToolTip("Insert current file name at cursor position")
        self.insert_filename_multi_all_qpb.clicked.connect(self.on_insert_current_filename_clicked)
        self.insert_filename_single_qpb = QtWidgets.QPushButton("Insert filename, single")
        vbox_l3.addWidget(self.insert_filename_single_qpb)
        self.insert_filename_single_qpb.clicked.connect(self.on_insert_filename_single_clicked)

        self.insert_filename_multi_random_qpb = QtWidgets.QPushButton(
            "Insert filename placeholder, multiple files, random")
        vbox_l3.addWidget(self.insert_filename_multi_random_qpb)
        self.insert_filename_multi_random_qpb.clicked.connect(self.on_insert_current_filename_random_clicked)

        vbox_l3.addSpacing(10)
        vbox_l3.addWidget(QtWidgets.QLabel("New Filename"))
        self.insert_new_filename_qpb = QtWidgets.QPushButton("Insert new (modified) filename")
        vbox_l3.addWidget(self.insert_new_filename_qpb)
        self.insert_new_filename_qpb.setToolTip("Insert new file name at cursor position")
        self.insert_new_filename_qpb.clicked.connect(self.on_insert_new_filename_clicked)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        self.remove_from_end_qle = QtWidgets.QLineEdit()
        hbox_l4.addWidget(self.remove_from_end_qle)
        self.remove_from_end_qle.setObjectName("remove_from_each_orig")
        self.remove_from_end_qle.setPlaceholderText("remove from end of each original file name")
        self.add_to_end_qle = QtWidgets.QLineEdit()
        hbox_l4.addWidget(self.add_to_end_qle)
        self.add_to_end_qle.setObjectName("add_to_each_orig")
        self.add_to_end_qle.setPlaceholderText("add to end of each file name")

        vbox_l3.addSpacing(10)

        vbox_l3.addWidget(QtWidgets.QLabel("Export"))
        self.export_to_desktop_qpb = QtWidgets.QPushButton("..to desktop file")
        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        hbox_l4.addWidget(self.export_to_desktop_qpb)
        self.export_to_desktop_qpb.clicked.connect(self.on_export_to_desktop)

        self.process_qgb = QtWidgets.QGroupBox("Process")
        vbox_l2.addWidget(self.process_qgb)
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.process_qgb.setLayout(vbox_l3)

        self.process_qpb = QtWidgets.QPushButton("Process")
        vbox_l3.addWidget(self.process_qpb)
        new_font.setPointSize(16)
        self.process_qpb.setFont(new_font)
        self.process_qpb.clicked.connect(self.on_run_clicked)

        self.ongoing_qprb = QtWidgets.QProgressBar()
        vbox_l3.addWidget(self.ongoing_qprb)

        vbox_l3.addWidget(QtWidgets.QLabel("Output"))
        self.output_qpe = QtWidgets.QPlainTextEdit()
        self.output_qpe.setReadOnly(True)
        vbox_l3.addWidget(self.output_qpe)

        # Dock widgets..

        # ..save and load

        self.save_and_load_dock = QtWidgets.QDockWidget("Save and load")
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.save_and_load_dock)
        self.save_and_load_widget = QtWidgets.QWidget()
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.save_and_load_widget.setLayout(vbox_l3)
        self.save_and_load_dock.setWidget(self.save_and_load_widget)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        self.save_qpb = QtWidgets.QPushButton("Save")
        hbox_l4.addWidget(self.save_qpb)
        self.save_qpb.clicked.connect(self.on_save_clicked)
        self.save_name_qle = QtWidgets.QLineEdit()
        hbox_l4.addWidget(self.save_name_qle)

        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        self.load_qpb = QtWidgets.QPushButton("Load")
        hbox_l4.addWidget(self.load_qpb)
        self.loaded_qll = QtWidgets.QLabel("")
        hbox_l4.addWidget(self.loaded_qll, stretch=1)

        self.delete_loaded_qpb = QtWidgets.QPushButton("Delete")
        hbox_l4.addWidget(self.delete_loaded_qpb)
        self.delete_loaded_qpb.clicked.connect(self.on_delete_loaded_clicked)

        self.load_qmenu = QtWidgets.QMenu()
        self.load_qpb.setMenu(self.load_qmenu)
        self.load_qmenu.triggered.connect(self.on_load_triggered)

        self.action_list_qlw = QtWidgets.QListWidget()
        vbox_l3.addWidget(self.action_list_qlw)

        # ..files

        self.files_dock_widget = QtWidgets.QDockWidget("Files")
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.files_dock_widget)
        self.files_widget = QtWidgets.QWidget()
        vbox_l3 = QtWidgets.QVBoxLayout()
        self.files_widget.setLayout(vbox_l3)
        self.files_dock_widget.setWidget(self.files_widget)

        self.recursive_qcb = QtWidgets.QCheckBox("Recursive")
        vbox_l3.addWidget(self.recursive_qcb)
        self.recursive_qcb.setObjectName("recursive_bool")
        self.recursive_qcb.toggled.connect(self.update_gui)

        vbox_l3.addWidget(QtWidgets.QLabel("Filters"))
        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        hbox_l4.addWidget(QtWidgets.QLabel("Pattern:"))
        self.fn_pattern_qle = QtWidgets.QLineEdit()
        hbox_l4.addWidget(self.fn_pattern_qle)
        self.fn_pattern_qle.setObjectName("pattern_fnmatch")
        self.fn_pattern_qle.setText("*")
        self.fn_pattern_qle.textChanged.connect(self.update_gui)
        hbox_l4 = QtWidgets.QHBoxLayout()
        vbox_l3.addLayout(hbox_l4)
        hbox_l4.addWidget(QtWidgets.QLabel("Min size:"))
        self.min_size_qsp = QtWidgets.QSpinBox()
        hbox_l4.addWidget(self.min_size_qsp)
        self.min_size_qsp.setObjectName("pattern_minsize_int")
        self.min_size_qsp.setMinimum(0)
        # self.min_size_qsp.setSingleStep(10)
        self.min_size_qsp.setMaximum(1000)
        self.min_size_qsp.setValue(0)
        self.min_size_qsp.valueChanged.connect(self.update_gui)
        self.min_size_type_qcb = QtWidgets.QComboBox()
        hbox_l4.addWidget(self.min_size_type_qcb)
        self.min_size_qsp.setObjectName("pattern_minsize_type")
        self.min_size_type_qcb.addItems([B_SIZE_STR, KB_SIZE_STR, MB_SIZE_STR, GB_SIZE_STR])
        self.min_size_type_qcb.setCurrentText(MB_SIZE_STR)
        self.min_size_type_qcb.currentIndexChanged.connect(self.update_gui)

        vbox_l3.addWidget(QtWidgets.QLabel("Files for processing"))
        self.files_for_processing_qlw = QtWidgets.QListWidget()
        vbox_l3.addWidget(self.files_for_processing_qlw)

        self.update_gui()
        self._set_progress_none()

        self.setup_value_funcs_dict = {
            self.pwd_qll.objectName(): SaveAndLoadInfo(
                self.pwd_qll.text, self.pwd_qll.setText, str),
            self.recursive_qcb.objectName(): SaveAndLoadInfo(
                self.recursive_qcb.isChecked, self.recursive_qcb.setChecked, bool),
            self.fn_pattern_qle.objectName(): SaveAndLoadInfo(
                self.fn_pattern_qle.text, self.fn_pattern_qle.setText, str),
            self.min_size_qsp.objectName(): SaveAndLoadInfo(
                self.min_size_qsp.value, self.min_size_qsp.setValue, int),
            self.min_size_type_qcb.objectName(): SaveAndLoadInfo(
                self.min_size_type_qcb.currentText, self.min_size_type_qcb.setCurrentText, str),
            self.command_line_qle.objectName(): SaveAndLoadInfo(
                self.command_line_qle.text, self.command_line_qle.setText, str),
            self.remove_from_end_qle.objectName(): SaveAndLoadInfo(
                self.remove_from_end_qle.text, self.remove_from_end_qle.setText, str),
            self.add_to_end_qle.objectName(): SaveAndLoadInfo(
                self.add_to_end_qle.text, self.add_to_end_qle.setText, str)
        }

        """
        self.setup_value_funcs_dict = {
            self.pwd_qll.objectName(): self.pwd_qll.text,
            self.recursive_qcb.objectName(): self.recursive_qcb.isChecked,
            self.fn_pattern_qle.objectName(): self.fn_pattern_qle.text,
            self.min_size_qsp.objectName(): self.min_size_qsp.value,
            self.min_size_type_qcb.objectName(): self.min_size_type_qcb.currentText,
            self.command_line_qle.objectName(): self.command_line_qle.text,
            self.remove_from_end_qle.objectName(): self.remove_from_end_qle.text,
            self.add_to_end_qle.objectName(): self.add_to_end_qle.text
        }
        """

        # self.save_cmd_setup("first setup")

    def on_terminal_action_triggered(self):
        os.system(f"exo-open --launch TerminalEmulator --working-directory {self.pwd_qll.text()}")
        # TODO: Fixing so this works cross-platform, alternatively storing custom in settings file

    def on_file_manager_action_triggered(self):
        os.system(f"exo-open --launch FileManager {self.pwd_qll.text()}")
        # TODO: Fixing so this works cross-platform, alternatively storing custom in settings file

    def on_export_to_desktop(self):
        desktop_template: str = """[Desktop Entry]
Name=${name}
Type=Application
Exec=${command}
Path=/home/sunyata/pycharm-community/bin
Terminal=true"""
        name_str, result_bool = QtWidgets.QInputDialog.getText(self, "Name", "Name of desktop shortcut")
        command_str = self.command_line_qle.text()

        template = Template(desktop_template)
        completed_desktop_text: str = template.substitute(
            name=name_str,
            command=f'"{command_str}"'
        )
        file_name: str = "file.desktop"
        with open(file_name, "w+") as f:
            f.write(completed_desktop_text)
        old_stat_mode = os.stat(file_name).st_mode
        new_stat_mode = old_stat_mode | stat.S_IEXEC
        os.chmod(file_name, new_stat_mode)

    def on_command_line_text_changed(self, i_new_text: str):
        self.update_gui()

    def _set_progress_none(self):
        self.ongoing_qprb.setRange(0, 1)
        self.ongoing_qprb.setValue(0)

    def _set_progress_processing(self, i_current: int, i_total: int):
        self.ongoing_qprb.setRange(0, i_total)
        self.ongoing_qprb.setValue(i_current)

    def on_delete_loaded_clicked(self):
        loaded_save: str = self.loaded_qll.text()
        if not loaded_save:
            QtWidgets.QMessageBox.warning("Nothing has been loaded")
        all_setups: dict = self.load_all_cmd_setups()
        all_setups.pop(loaded_save)
        with open(SAVE_FILE_NAME_STR, "w") as f:
            json.dump(all_setups, f)
        self.loaded_qll.clear()
        self.update_gui()

    """
    def on_open_with_triggered(self, i_menu_action: QtWidgets.QAction):
        i_menu_action.
    """

    def on_load_triggered(self, i_menu_action: QtWidgets.QAction):
        save_name: str = i_menu_action.text()
        print(f"{save_name=}")
        self.load_cmd_setup(save_name)
        self.loaded_qll.setText(save_name)

    def on_save_clicked(self):
        save_name: str = self.save_name_qle.text()
        if not save_name:
            QtWidgets.QMessageBox.warning(self, "No name given for the save", "No name given for the save")
            return
        self.save_name_qle.clear()
        self.save_cmd_setup(save_name)
        self.update_gui()

    def save_cmd_setup(self, i_setup_name: str):
        # dict[str, SaveAndLoadInfo]
        new_setup: dict = {}
        for key, info in self.setup_value_funcs_dict.items():
            value = info.get_func()
            new_setup[key] = value

        setup_collections: dict = self.load_all_cmd_setups()  # -old setup
        setup_collections[i_setup_name] = new_setup  # -adding new
        with open(SAVE_FILE_NAME_STR, "w") as f:
            json.dump(setup_collections, f)

    def load_all_cmd_setups(self):
        try:
            with open(SAVE_FILE_NAME_STR, "r") as f:
                all_data_loaded_dict = json.load(f)
                return all_data_loaded_dict
        except Exception:
            return {}

    def load_cmd_setup(self, i_setup_name: str):
        all_data: dict = self.load_all_cmd_setups()
        setup_to_load: dict = all_data[i_setup_name]
        for obj_name, value in setup_to_load.items():
            info_obj = self.setup_value_funcs_dict[obj_name]
            info_obj.set_func(value)
            # widget = self.findChild(obj_name)

        # self.pwd_qll.setText(setup_to_load["root_dir"])
        # self.recursive_qcb.setChecked(_)
        # self.fn_pattern_qle.setText(_)

    def show_all_cmd_setups(self):
        pass

    def on_set_pwd_clicked(self):
        dir_str = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Select directory", self.pwd_qll.text(), QtWidgets.QFileDialog.ShowDirsOnly
        )
        self.pwd_qll.setText(dir_str)

        self.update_gui()

    def filter_file(self, i_directory: str, i_df_name: str, i_pattern: str):
        old_full_path_str = os.path.join(i_directory, i_df_name)
        if fnmatch.fnmatch(i_df_name, i_pattern) and os.path.isfile(old_full_path_str):

            min_size_int = self.min_size_qsp.value()
            if self.min_size_type_qcb.currentText() == KB_SIZE_STR:
                min_size_in_bytes_int = 1024 * min_size_int
            elif self.min_size_type_qcb.currentText() == MB_SIZE_STR:
                min_size_in_bytes_int = 1024 ** 2 * min_size_int
            elif self.min_size_type_qcb.currentText() == GB_SIZE_STR:
                min_size_in_bytes_int = 1024 ** 3 * min_size_int
            else:
                min_size_in_bytes_int = min_size_int
            file_size_int = os.path.getsize(old_full_path_str)
            if file_size_int < min_size_in_bytes_int:
                return None

            new_file_name_path_str = remove_suffix(
                old_full_path_str,
                self.remove_from_end_qle.text()
            )
            new_file_name_path_str += self.add_to_end_qle.text()

            file_path_combo_obj = FilePathCombo(old_full_path_str, new_file_name_path_str)
            return file_path_combo_obj
        return None

    def get_filtered_file_names(self) -> [FilePathCombo]:
        directory_str = self.pwd_qll.text()
        pattern_str = self.fn_pattern_qle.text()
        if not pattern_str.strip():
            return []  # -to save time
        file_combo_list = []
        if self.recursive_qcb.isChecked():
            for root, _, filename_list in os.walk(directory_str):
                if len(filename_list) > FILE_NR_LIMIT:
                    raise Exception(f"Over file number limit ({FILE_NR_LIMIT})")
                for fn in filename_list:
                    file_path_combo_obj = self.filter_file(root, fn, pattern_str)
                    if file_path_combo_obj:
                        file_combo_list.append(file_path_combo_obj)
        else:
            for df_name in os.listdir(directory_str):
                file_path_combo_obj = self.filter_file(directory_str, df_name, pattern_str)
                if file_path_combo_obj:
                    file_combo_list.append(file_path_combo_obj)
        return file_combo_list

    def update_gui(self):
        file_combo_list: [FilePathCombo] = self.get_filtered_file_names()
        self.files_for_processing_qlw.clear()
        self.files_for_processing_qlw.addItems([fn.get_old_name() for fn in file_combo_list])
        all_cmd_setups: dict = self.load_all_cmd_setups()
        self.load_qmenu.clear()
        for save_name in all_cmd_setups:
            self.load_qmenu.addAction(save_name)

        self.action_list_qlw.clear()
        self.action_list_qlw.addItems(all_cmd_setups)

        command_str = self.command_line_qle.text()
        if CURRENT_FILE_NAME_STR in command_str:
            self.files_dock_widget.setEnabled(True)
        elif FILE_NAME_RANDOM_STR in command_str:
            self.files_dock_widget.setEnabled(True)
        else:
            self.files_dock_widget.setEnabled(False)

    def on_insert_filename_single_clicked(self):
        print("cursor pos: " + str(self.command_line_qle.cursorPosition()))
        file_path, result = QtWidgets.QFileDialog.getOpenFileName()
        # TODO??? getOpenFileNames (multiple files)
        self.command_line_qle.insert(file_path)

    def on_insert_current_filename_random_clicked(self):
        self.command_line_qle.insert(FILE_NAME_RANDOM_STR)

    def on_insert_current_filename_clicked(self):
        print("cursor pos: " + str(self.command_line_qle.cursorPosition()))
        self.command_line_qle.insert(CURRENT_FILE_NAME_STR)

    def on_insert_new_filename_clicked(self):
        print("cursor pos: " + str(self.command_line_qle.cursorPosition()))
        self.command_line_qle.insert(NEW_FILE_NAME_STR)

    def get_completed_commands(self):  # sourcery skip: move-assign
        completed_command_list = []
        command_str = self.command_line_qle.text()
        if CURRENT_FILE_NAME_STR in command_str:
            for fnc in self.get_filtered_file_names():
                template = Template(command_str)
                completed_command_str = template.substitute(
                    current_file_name=f'"{fnc.get_old_path()}"',
                    new_file_name=f'"{fnc.get_new_path()}"'
                )
                completed_command_list.append(completed_command_str)
            return completed_command_list
        elif FILE_NAME_RANDOM_STR in command_str:
            fnc_random = random.choice(self.get_filtered_file_names())
            template = Template(command_str)
            completed_command_str = template.substitute(
                file_name_random=f'"{fnc_random.get_old_path()}"',
                new_file_name=f'"{fnc_random.get_new_path()}"'
            )
            return [completed_command_str]
        else:
            return [command_str]

    def on_run_clicked(self):
        self.output_qpe.clear()
        completed_commands: list[str] = self.get_completed_commands()
        nr_of_files: int = len(completed_commands)
        for i, cc_str in enumerate(completed_commands):
            self.output_qpe.appendPlainText(f"===== Running command {cc_str} =====")
            try:
                output_str = subprocess.check_output(
                    cc_str,
                    stderr=subprocess.STDOUT,
                    shell=True,
                    text=True,
                    cwd=self.pwd_qll.text()
                )
                self.output_qpe.appendPlainText(output_str)
                self._set_progress_processing(i, nr_of_files)
            except subprocess.CalledProcessError:
                QtWidgets.QMessageBox.information(self, "CalledProcessError",
                    f"CalledProcessError for command {cc_str}")
        self.output_qpe.appendPlainText("===== All done <3 =====")
        self._set_progress_none()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main_window = MyMainWindow()

    main_window.show()
    app.exec_()
