

TODO: Setup.py so it can be installed with pip3 install -e

# Ideas

Presentation:
* each command has a button
* each command can have a notification which can activate it
* each command can be activated at a certain time

* Showing commands in systray
  * pinned command, pinned to systray menu
  * shown at a certain time - with option of running a command - at certain times (hour/day) or intervals

Running commands at certain times (hour/day) or intervals

Running commands continously (every minute) with a changing parameter
gradual change (using a parameter)

running python commands
python -m [command] args

Can both remembering and repeating_cmd be covered?

Running multiple commands of different types: using "&&" or using a script
Examples: Backup+tree_cmd

Lazy Linux Commands d* post: https://diasp.eu/posts/12058391

Export to shortcut file:
1. x .desktop file
2. shell script
3. python script
